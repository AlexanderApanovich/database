from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
#from flask_migrate import Migrate

#from sqlalchemy import create_engine
#engine = create_engine('mysql://root:123456@127.0.0.1:3306/e-courses', echo=True)

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
#migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'index'
login.login_message = "Пожалуйста, войдите, чтобы открыть эту страницу."
bootstrap = Bootstrap(app)

from app import routes, models
#импорт из папки app

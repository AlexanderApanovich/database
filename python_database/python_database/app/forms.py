from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField,DateTimeField, FieldList, FormField,IntegerField,TextAreaField, SelectField
from wtforms.validators import DataRequired
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app import db
from app.models import *
from sqlalchemy import or_
from flask_login import current_user

def q1():
    return Users.query.filter(Users.Name!=None).order_by(Users.Name)

def q20():
    return Tasks.query.filter(or_(current_user.Id == Tasks.Reviewer, current_user.Id == Tasks.Developer))

def q2():
    return Departments.query.order_by(Departments.Name)

def q3():
    return task_types.query.order_by(task_types.Name)

def q4():
    return Blocks.query.order_by(Blocks.Name)

def q5():
    return Users.query.filter(Users.Name!=None).filter(Users.Name!='konovalov').filter(Users.Name!='shneiderov').order_by(Users.Name)


class LoginForm(FlaskForm):
    name = QuerySelectField('Логин', query_factory=q1, get_label="Name") #конструктор: StringField(Label,validators)
    password = PasswordField('Пароль', validators=[DataRequired()])
    submit = SubmitField('Вход')

class TrackingForm(FlaskForm):
    name = QuerySelectField('Задание',query_factory=q20,get_label="Name") 
    comment = TextAreaField('Комментарий', validators=[DataRequired()])
    submit = SubmitField('Отправить')

class ChooseDepartment(FlaskForm):
    department = QuerySelectField('Фильтровать по кафедре:', query_factory=q2,get_label="Name")
    submit = SubmitField('Фильтр')

class AddTask(FlaskForm):
    course = QuerySelectField('Дисциплина', get_label='Name')
    name = TextAreaField('Название', validators=[DataRequired()])
    type = QuerySelectField('Тип', query_factory=q3,get_label='Name')
    developer = QuerySelectField('Разработчик', query_factory=q5, get_label="Name")
    reviewer = QuerySelectField('Проверяющий', query_factory=q5, get_label="Name")
    comment = TextAreaField('Комментарий (опционально)')
    submit = SubmitField('Принять')

class ChooseBlock(FlaskForm):
    block = QuerySelectField('', query_factory=q4,get_label='Name')
    submit = SubmitField('Принять')
    reset = SubmitField('Сброс')

class ChooseUser(FlaskForm):
    user = QuerySelectField('', query_factory=q5,get_label='Name')
    submit = SubmitField('Принять')
    reset = SubmitField('Сброс')
    
        

#class DataBaseForm(FlaskForm):
    
#    id = IntegerField()

# #   eord = StringField()
# #   task = StringField()
# #   assign_date = DateTimeField()
# #   type = StringField()
# #   accept_date = DateTimeField()
#    accepted = SubmitField(label = 'Принять')#, description = 'Принять')
##    comment = StringField()
#    completed = SubmitField(label = 'Завершить')#, description = None)
#   # completed_date = SubmitField()

#    def set_values(self, task, user):
#        self.eord.description = task.course.FullName
#        self.task.description = task.Name
#        self.assign_date.description = task.Created
#     #   self.type.description = task.status.Name

#        if (user.Id == task.Developer):
#            if(task.EndDevelop):
#                self.accept_date.description = task.EndDevelop
#                self.accepted.description = ""
#            else:
#                self.accepted.description = 'Принять'

#        if (user.Id == task.Reviewer):
#            if(task.EndReview):
#                self.accept_date.description = task.EndReview
#                self.accepted.description = ""
#            else:
#                self.accepted.description = 'Принять'
#                self.comment.description = 'Комментарий'

#        if(task.EndReview and task.EndDevelop):
#            self.completed.description = 'Завершить'

#    #def set_start_develop_time():

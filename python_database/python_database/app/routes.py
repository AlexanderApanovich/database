from flask import render_template, flash, redirect,url_for, request
from app import app
from app.forms import *
from app.models import *
from app import db
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from sqlalchemy import or_,and_, text,func
import datetime

@app.route('/', methods=['GET','POST'])
@app.route('/index', methods=['GET','POST'])
def index():    
    #если пользователь уже аутентифицирован - перенаправляем на НЕОБХОДИМУЮ страницу
    if current_user.is_authenticated:
        if current_user.IsAdmin:
            return redirect(url_for('admin_approve'))
        return redirect(url_for('database'))
    #экземпляр класса LoginForm
    form = LoginForm()
    #если все поля проверены
    if form.validate_on_submit():
        #находим пользователя в БД
        user = Users.query.filter_by(Id=form.name.data.Id).first()
        #если пользователя там нет или нет хеша или хеш не совпадает
        if user is None or user.Password_hash == None or not user.check_password(form.password.data):
            flash('Неправильный логин/пароль')
            return redirect(url_for('index'))
        #логин
        login_user(user, remember=False)
        #перенаправление на запрошенную ранее страницу
        next_page = request.args.get('next')
        #проверка на злоумышленника
        if not next_page or url_parse(next_page).netloc != '':
            if current_user.IsAdmin:
                next_page = url_for('admin_approve')
            else:
                next_page = url_for('database')
        #if next_page == '/admin':
        #    if current_user.isAdmin == False:
        #        next_page = url_for('index')
        return redirect(next_page)
    return render_template('index.html', title='Вход', form=form)

@app.route('/database', methods=['GET','POST'])
@login_required
def database():
    page = request.args.get('page', 1, type=int)
    tasks_per_page = app.config['TASKS_PER_PAGE']
    accept_task = False

    #подзапрос для максимальной даты
    latest_tracking = db.session.query(
        Tracking.User,
        Tracking.Task,
        func.max(Tracking.Id).label('max_time'),
    ).filter_by(User = current_user.Id).group_by(Tracking.Task).subquery('latest_tracking')

    #все последние трекинги по каждому заданию
    all_trackings = db.session.query(Tracking).filter(and_(
        Tracking.User == latest_tracking.c.User,
        Tracking.Task == latest_tracking.c.Task,
        Tracking.Id == latest_tracking.c.max_time,
    ))

    #трекинги с ненулевой датой начала
    completed_trackings = db.session.query(Tracking).filter(Tracking.User == current_user.Id).filter(Tracking.StartTime != None)   
    completed = completed_trackings.count()

    all_trackings = all_trackings.union(completed_trackings).order_by(Tracking.Status.asc(), Tracking.Time.desc())

    #для проверки наличия выполнения задания  
    accept_task = True
    for tracking in all_trackings.all():
        if tracking.status.Name in ('Переносится', 'Проверяется', 'Дорабатывается'):
            accept_task = False
               
    #расчет количества страниц
    total = all_trackings.count()
    if total / tasks_per_page - total // tasks_per_page > 0:
        pages_count = total // tasks_per_page + 1
    else:
        pages_count = total // tasks_per_page

    #пагинация и подсчет некоторых данных
    trackings = all_trackings.paginate(page, tasks_per_page, False)
    ids = []
    i = (page - 1) * tasks_per_page
    for tracking in trackings.items:
        #для номера в таблице
        i = i + 1
        tracking.number = i
        #для проверки на подмену id кнопки
        ids.append(tracking.Id)        
        
    next_url = url_for('database', page=trackings.next_num) \
        if trackings.has_next else None
    prev_url = url_for('database', page=trackings.prev_num) \
        if trackings.has_prev else None
    
    if request.method == 'POST':                                      
        #принятие задания
        if request.form.get('Accept'):
            #проверка на подмену id
            if not int(request.form.get('Accept')) in ids:
                raise Exception()#str(request.form.get('Accept')) + " not in "+ str(ids))

            tracking = Tracking.query.filter_by(Id = request.form.get('Accept')).first()
            if tracking.status.Name == "Не перенесён":
                next_status = "Переносится"
            if tracking.status.Name == "Не проверен":
                next_status = "Проверяется"
            if tracking.status.Name == "Не доработан":
                next_status = "Дорабатывается"
            
            next_status_id = Statuses.query.filter_by(Name = next_status).first().Id

            accepted_tracking = Tracking(Task = tracking.Task, Status = next_status_id, User = current_user.Id, Time = datetime.datetime.now(), Comment =request.form.get('comment_accept'))
            db.session.add(accepted_tracking)
            db.session.commit()

        #завершение задания
        if request.form.get('Complete'):
            #проверка на подмену id
            if not int(request.form.get('Complete')) in ids:
                raise Exception()#str(request.form.get('Complete')) + " not in "+ str(ids))

            tracking = Tracking.query.filter_by(Id = request.form.get('Complete')).first()
            if tracking.status.Name == "Переносится":
                next_status = "Перенесён"                                
            if tracking.status.Name == "Проверяется":
                next_status = "Проверен"
            if tracking.status.Name == "Дорабатывается":
                next_status = "Доработан"
            
            next_status_id = Statuses.query.filter_by(Name = next_status).first().Id

            accepted_tracking = Tracking(Task = tracking.Task, Status = next_status_id, User = current_user.Id, \
                Time = datetime.datetime.now(), StartTime = tracking.Time, Comment =request.form.get('comment_complete'))
            db.session.add(accepted_tracking)
            db.session.commit()
            
            if tracking.status.Name == "Переносится":
                start_review_status_id = Statuses.query.filter_by(Name = "Не проверен").first().Id
                toreview_tracking = Tracking(Task = tracking.Task, Status = start_review_status_id, User = tracking.task.Reviewer, \
                    Time = datetime.datetime.now() + datetime.timedelta(0, 2), Comment =request.form.get('comment_complete'))
                db.session.add(toreview_tracking)
                db.session.commit()

            if tracking.status.Name in ("Проверяется","Дорабатывается"):
                admin_id = Users.query.filter_by(Name = "konovalov").first().Id

                start_approving_status_id = Statuses.query.filter_by(Name = "Не утверждён").first().Id
                to_approve_tracking = Tracking(Task = tracking.Task, Status = start_approving_status_id, User = admin_id, \
                    Time = datetime.datetime.now()+ datetime.timedelta(0, 2), Comment =request.form.get('comment_complete'))

                db.session.add(to_approve_tracking)
                db.session.commit()
                
            
        #отправление на переработку
        if request.form.get('Decline'):
            #проверка на подмену id
            if not int(request.form.get('Decline')) in ids:
                raise Exception()#str(request.form.get('Decline')) + " not in "+ str(ids))
            tracking = Tracking.query.filter_by(Id = request.form.get('Decline')).first()
            
            accepted_status_id = Statuses.query.filter_by(Name = "Проверен").first().Id
            accepted_tracking = Tracking(Task = tracking.Task, Status = accepted_status_id, User = current_user.Id, Time = datetime.datetime.now(), StartTime = tracking.Time, Comment =request.form.get('comment_complete'))

            need_rework_status_id = Statuses.query.filter_by(Name = "Не доработан").first().Id
            rework_tracking = Tracking(Task = tracking.Task, Status = need_rework_status_id, User = tracking.task.Developer, Time = datetime.datetime.now()+ datetime.timedelta(0, 2), Comment =request.form.get('comment_complete'))

            db.session.add(accepted_tracking)
            db.session.commit()

            db.session.add(rework_tracking)
            db.session.commit()
        return redirect(url_for('database',page=page))
                        
    return render_template('database.html',completed=completed,count=total,accept_task=accept_task,pages_count=pages_count, current_page=page, trackings = trackings.items,user = current_user,next_url=next_url, prev_url=prev_url,)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

import plotly
import plotly.plotly as py
import plotly.figure_factory as ff
import json

@app.route('/diagram')
@login_required
def diagram():
    now = datetime.datetime.now()

    #подзапрос для максимальной даты
    latest_tracking = db.session.query(
        Tracking.User,
        Tracking.Task,
        func.max(Tracking.Id).label('max_time'),
    ).filter_by(User = current_user.Id).group_by(Tracking.Task).subquery('latest_tracking')

    #все последние трекинги по каждому заданию
    all_trackings = db.session.query(Tracking).filter(and_(
        Tracking.User == latest_tracking.c.User,
        Tracking.Task == latest_tracking.c.Task,
        Tracking.Id == latest_tracking.c.max_time,
    ))

    #трекинги с ненулевой датой начала
    completed_trackings = db.session.query(Tracking).filter(Tracking.User == current_user.Id).filter(Tracking.StartTime != None)
    all_trackings = all_trackings.union(completed_trackings).order_by(Tracking.Status.asc(), Tracking.Time.asc())

    current = ''
    df = []
    for tracking in all_trackings:
        if tracking.StartTime:
            start_time = tracking.StartTime
            end_time = tracking.Time           
        else:
            if tracking.status.Name in ('Дорабатывается','Переносится','Проверяется'):
                start_time = tracking.Time
                end_time = datetime.datetime.now()
                current = '(текущее)'
            else:
                continue

        delta_time = round((end_time-start_time).total_seconds()/60)

        if tracking.status.Name in ('Дорабатывается', 'Доработан', 'Не доработан'):
            res = 'Доработка'
        if tracking.status.Name in ('Переносится', 'Перенесён', 'Не перенесён'):
            res = 'Перенос'
        if tracking.status.Name in ('Проверяется', 'Проверен', 'Не проверен'):
            res = 'Проверка'

        df.append({'Task' : tracking.task.Name, \
            'Start' : start_time.strftime("%Y-%m-%d %H:%M"), 'Finish': end_time.strftime("%Y-%m-%d %H:%M"),\
            'Resource' : res, 'Description' : 'Длительность: '+ str(delta_time)+' минут ' + current})    

    if df == []:
        flash('У вас еще нет готовых/текущих заданий для отображения!')
        return redirect(url_for('index'))
    
    colors = {'Доработка': 'rgb(196, 6, 6)',
              'Перенос' : 'rgb(0, 0, 255)',
              'Проверка' : 'rgb(255, 179, 0)'}
    
    fig = ff.create_gantt(df,colors=colors, index_col='Resource', title = '', show_colorbar=True, showgrid_x=True, showgrid_y=True,\
        group_tasks=True)
    fig['layout'].update(autosize=True, margin=dict(l=200))
    #fig['layout'].update(autosize=False, width=1300, height=700, margin=dict(l=200))
    
    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    return render_template('diagram.html',graph=graphJSON)#,trackings=all_trackings,now=now,user = current_user)

#@app.route('/tracking', methods=['GET','POST'])
#@login_required
#def tracking():
#    form = TrackingForm()
#    if form.validate_on_submit():
#        tracking = Tracking(Task=form.name.data.Id,Author=current_user.Id,Comment=form.comment.data,DateTime=datetime.datetime.now())
#        db.session.add(tracking)
#        db.session.commit()
#        return redirect(url_for('tracking'))
#    return render_template('tracking.html', form=form,user = current_user)

@app.route('/admin/approve', methods=['GET','POST'])
@login_required
def admin_approve():
    if not current_user.IsAdmin:
        return redirect(url_for('index'))
    page = request.args.get('page', 1, type=int)
    tasks_per_page = app.config['TASKS_PER_PAGE']

    not_approved_id = Statuses.query.filter_by(Name = 'Не утверждён').first().Id
    approved_id = Statuses.query.filter_by(Name = 'Утверждён').first().Id

    #подзапрос для максимальной даты
    latest_tracking = db.session.query(
        Tracking.User,
        Tracking.Task,
        func.max(Tracking.Id).label('max_time'),
    ).filter_by(User = current_user.Id).group_by(Tracking.Task).subquery('latest_tracking')

    #все последние трекинги по каждому заданию
    all_trackings = db.session.query(Tracking).filter(and_(
        Tracking.User == latest_tracking.c.User,
        Tracking.Task == latest_tracking.c.Task,
        Tracking.Id == latest_tracking.c.max_time,
    ))

    #трекинги с ненулевой датой начала
    completed_trackings = db.session.query(Tracking).filter(Tracking.User == current_user.Id).filter(Tracking.StartTime != None)   
    completed = completed_trackings.count()

    all_trackings = all_trackings.union(completed_trackings).order_by(Tracking.Status.asc())

    #расчет количества страниц
    total = all_trackings.count()
    if total / tasks_per_page - total // tasks_per_page > 0:
        pages_count = total // tasks_per_page + 1
    else:
        pages_count = total // tasks_per_page

    #пагинация и подсчет некоторых данных
    trackings = all_trackings.paginate(page, tasks_per_page, False)
    i = (page - 1) * tasks_per_page
    for tracking in trackings.items:
        #для номера в таблице
        i = i + 1
        tracking.number = i 
    
    if request.method == 'POST':                                      
        #принятие задания
        if request.form.get('Accept'):
            tracking = Tracking.query.filter_by(Id = request.form.get('Accept')).first()
            
            approved_id = Statuses.query.filter_by(Name = 'Утверждён').first().Id
            accepted_tracking = Tracking(Task = tracking.Task, Status = approved_id, User = current_user.Id, Time = datetime.datetime.now(), StartTime = datetime.datetime.now(), Comment =request.form.get('comment'))
            
            db.session.add(accepted_tracking)
            db.session.commit()
                                                              
        #принятие задания
        if request.form.get('Decline'):
            tracking = Tracking.query.filter_by(Id = request.form.get('Decline')).first()

            approving_id = Statuses.query.filter_by(Name = 'Утверждается').first().Id
            approving_tracking = Tracking(Task = tracking.Task, Status = approving_id, User = current_user.Id, Time = datetime.datetime.now(), Comment =request.form.get('comment'))

            rework_id = Statuses.query.filter_by(Name = 'Не доработан').first().Id
            accepted_tracking = Tracking(Task = tracking.Task, Status = rework_id, User = tracking.task.Developer, Time = datetime.datetime.now()+ datetime.timedelta(0, 2), Comment =request.form.get('comment'))
            
            db.session.add(approving_tracking)
            db.session.commit()
            db.session.add(accepted_tracking)
            db.session.commit()

        if request.form.get('Review'):
            tracking = Tracking.query.filter_by(Id = request.form.get('Review')).first()
            
            approving_id = Statuses.query.filter_by(Name = 'Утверждается').first().Id
            approving_tracking = Tracking(Task = tracking.Task, Status = approving_id, User = current_user.Id, Time = datetime.datetime.now(), Comment =request.form.get('comment'))

            review = Statuses.query.filter_by(Name = 'Не проверен').first().Id
            accepted_tracking = Tracking(Task = tracking.Task, Status = review, User = tracking.task.Reviewer, Time = datetime.datetime.now()+ datetime.timedelta(0, 2), Comment =request.form.get('comment'))
            
            db.session.add(approving_tracking)
            db.session.commit()
            db.session.add(accepted_tracking)
            db.session.commit()
        
        return redirect(url_for('admin_approve',page=page))


    return render_template('approve.html',completed=completed,count=total,pages_count=pages_count, current_page=page, trackings = trackings.items,user = current_user)

@app.route('/admin/add', methods=['GET','POST'])
@login_required
def admin_add():
    if not current_user.IsAdmin:
        return redirect(url_for('index'))
    
    department = request.args.get('department',0, type=int)
    form_choose = ChooseDepartment()
    form_add = AddTask()
    
    if form_choose.validate_on_submit():
        department = form_choose.department.data.Id
        form_add.course.query = Courses.query.filter_by(Department = department).order_by(Courses.Name.asc())
        return redirect(url_for('admin_add', department=department))                                
    
    if department == 0:
        form_add.course.query = Courses.query.order_by(Courses.Name.asc())
    else:
        form_add.course.query = Courses.query.filter_by(Department = department).order_by(Courses.Name.asc())

    if form_add.validate_on_submit():
        if request.method == 'POST':

            latest = Tasks.query.order_by(Tasks.Id.desc()).first()
            if latest:
                latest_id = latest.Id
                latest_id += 1
            else: 
                latest_id = 1
            
            not_started_working_id = Statuses.query.filter_by(Name = 'Не перенесён').first().Id

            add_task = Tasks(Id = latest_id, Name = form_add.name.data, Type = form_add.type.data.Id, Course = form_add.course.data.Id, Developer = form_add.developer.data.Id, Reviewer = form_add.reviewer.data.Id)
            add_tracking = Tracking(Task = latest_id, Status = not_started_working_id, User = form_add.developer.data.Id, Time = datetime.datetime.now(), Comment =form_add.comment.data)

            db.session.add(add_task)
            db.session.commit()

            db.session.add(add_tracking)
            db.session.commit()

            flash('Задание успешно добавлено!')

            return redirect(url_for('admin_add'))
    
    return render_template('add.html', form_add=form_add, form_choose=form_choose, title='Админ')

@app.route('/admin/blockinfo', methods=['GET','POST'])
@login_required
def blockinfo():
    if not current_user.IsAdmin:
        return redirect(url_for('index'))
    
    block = request.args.get('block',0, type=int)

    form = ChooseBlock()
    
    if form.submit.data or form.reset.data:
        if form.submit.data:
            block = form.block.data.Id
        if form.reset.data:
            block = 0
        return redirect(url_for('blockinfo', block=block))

    if block == 0:
        block_name = None
        courseblocks = db.session.query(course_block).join(Courses).join(Blocks).order_by(Courses.Name.asc()).all()
    else:
        block_name = db.session.query(Blocks).get(block).Name
        courseblocks = db.session.query(course_block).join(Courses).join(Blocks).filter(Blocks.Id == block).order_by(Courses.Name.asc()).all()

    i = 0
    for courseblock in courseblocks:
        i = i + 1
        courseblock.number = i

    if request.method == 'POST':
        if request.form.get('info'):
            return redirect(url_for('courseinfo',course=request.form.get('info')))
        if request.form.get('all_info'):
            return redirect(url_for('courseinfo',course=0,block=block))
    
    return render_template('blockinfo.html',block_name=block_name, form=form,courseblocks = courseblocks,user = current_user)

@app.route('/admin/courseinfo', methods=['GET','POST'])
@login_required
def courseinfo():
    if not current_user.IsAdmin:
        return redirect(url_for('index'))
    
    course = request.args.get('course',0, type=int)

    if course == 0:
        block = request.args.get('block',type=int)
        block_name = db.session.query(Blocks).get(block).Name

        course_name = None

        latest_tracking = db.session.query(
            Tracking.Task,
            func.max(Tracking.Id).label('max_time'),
        ).join(Tasks).join(Courses).join(course_block).join(Blocks).filter(Blocks.Id == block).group_by(Tracking.Task).subquery('latest_tracking')
    else:
        block_name = None
        course_name = db.session.query(Courses).get(course).FullName

        latest_tracking = db.session.query(
            Tracking.Task,
            func.max(Tracking.Id).label('max_time'),
        ).join(Tasks).join(Courses).filter(Courses.Id == course).group_by(Tracking.Task).subquery('latest_tracking')

    tasks = db.session.query(Tracking).filter(and_(
        Tracking.Task == latest_tracking.c.Task,
        Tracking.Id == latest_tracking.c.max_time,
    )).join(Tasks).join(Courses).order_by(Courses.Name.asc(),Tasks.Name.asc()).group_by(Tracking.Task).all()

    i = 0
    for task in tasks:
        i = i + 1
        task.number = i

    if request.method == 'POST':
        if request.form.get('info'):
            return redirect(url_for('taskinfo',task=request.form.get('info')))
    
    return render_template('courseinfo.html', block_name=block_name, course_name=course_name, trackings = tasks,user = current_user)

@app.route('/admin/taskinfo', methods=['GET','POST'])
@login_required
def taskinfo():
    if not current_user.IsAdmin:
        return redirect(url_for('index'))
    
    task = request.args.get('task',type=int)
    
    trackings = db.session.query(Tracking).filter_by(Task = task).order_by(Tracking.Time.desc()).all()

    return render_template('taskinfo.html', trackings = trackings,user = current_user)

def to_minutes(t):
    return str(round((t).total_seconds()/60))

@app.route('/admin/usersinfo', methods=['GET','POST'])
@login_required
def usersinfo():
    if not current_user.IsAdmin:
        return redirect(url_for('index'))
    
    user = request.args.get('user',0, type=int)

    form = ChooseUser()
    
    if form.submit.data or form.reset.data:
        if form.submit.data:
            user = form.user.data.Id
        if form.reset.data:
            user = 0
        return redirect(url_for('usersinfo', user=user))

   # admin_id1 = Users.query.filter_by(Name = "konovalov").first().Id
   # admin_id2 = Users.query.filter_by(Name = "shneiderov").first().Id
   
    if user == 0:
        user_name = None
        user_list = db.session.query(Users).filter(Users.Name != "konovalov").filter(Users.Name != "shneiderov").order_by(Users.Name).all()
    else:
        user_name = db.session.query(Users).get(user).Name
        user_list = db.session.query(Users).filter(Users.Id == user).all()

    worked_id = Statuses.query.filter_by(Name = 'Перенесён').first().Id
    reviewed_id = Statuses.query.filter_by(Name = 'Проверен').first().Id
    reworked_id = Statuses.query.filter_by(Name = 'Доработан').first().Id

    for user in user_list:
        latest_tracking = db.session.query(
            Tracking.User,
            Tracking.Task,
            func.max(Tracking.Id).label('max_time'),
        ).filter_by(User = user.Id).group_by(Tracking.Task).subquery('latest_tracking')

        all_trackings = db.session.query(Tracking).filter(and_(
            Tracking.User == latest_tracking.c.User,
            Tracking.Task == latest_tracking.c.Task,
            Tracking.Id == latest_tracking.c.max_time,
        )).all()

        for tracking in all_trackings:
            if tracking.status.Name in ('Дорабатывается','Переносится','Проверяется'):
                user.current_task_time = to_minutes(datetime.datetime.now()-tracking.Time)
                user.current_task_type = tracking.task.type.Name
                user.current_task_course = tracking.task.course.Name
                user.current_task_status = tracking.status.Name
                user.current_task_name = tracking.task.Name

        #количество
        user.work_count = db.session.query(Tracking).filter(Tracking.User == user.Id).\
            filter(Tracking.StartTime != None).filter(Tracking.Status == worked_id).count()

        user.review_count = db.session.query(Tracking).filter(Tracking.User == user.Id).\
            filter(Tracking.StartTime != None).filter(Tracking.Status == reviewed_id).count()

        user.rework_count = db.session.query(Tracking).filter(Tracking.User == user.Id).\
            filter(Tracking.StartTime != None).filter(Tracking.Status == reworked_id).count()

        #трудоемкость
        work_time_tracking = db.session.query(Tracking).filter(Tracking.User == user.Id).\
            filter(Tracking.StartTime != None).filter(Tracking.Status == worked_id).all()
        user.work_time = datetime.timedelta()
        for time in work_time_tracking:
            user.work_time += time.Time - time.StartTime
        user.work_time = to_minutes(user.work_time)

        review_time_tracking = db.session.query(Tracking).filter(Tracking.User == user.Id).\
            filter(Tracking.StartTime != None).filter(Tracking.Status == reviewed_id).all()
        user.review_time = datetime.timedelta()
        for time in review_time_tracking:
            user.review_time += time.Time - time.StartTime
        user.review_time = to_minutes(user.review_time)

        rework_time_tracking = db.session.query(Tracking).filter(Tracking.User == user.Id).\
            filter(Tracking.StartTime != None).filter(Tracking.Status == reworked_id).all()
        user.rework_time = datetime.timedelta()
        for time in rework_time_tracking:
            user.rework_time += time.Time - time.StartTime
        user.rework_time = to_minutes(user.rework_time)
   
    return render_template('usersinfo.html',user_name=user_name,user_list=user_list,form=form,user = current_user)

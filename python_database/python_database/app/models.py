#перепроверить все столбцы
from app import db
from datetime import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login
from sqlalchemy.orm import relationship


class Users(UserMixin, db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50), index=True)
    IsAdmin = db.Column(db.Boolean)
    Password_hash = db.Column(db.String(128))
    
    Task_developer = db.relationship('Tasks',backref='developer',primaryjoin='Tasks.Developer == Users.Id')
    Task_reviewer = db.relationship('Tasks',backref='reviewer', primaryjoin='Tasks.Reviewer == Users.Id')
    Tracking = db.relationship('Tracking',backref='user',primaryjoin='Tracking.User == Users.Id')
    
    #def __repr__(self):
    #    return '<User: id = {}, name = {}, isAdmin = {}, password_hash = {}, trackings = {}>'.format(self.Id,self.Name,self.IsAdmin,self.Password_hash,self.Trackings)
    
    def set_password(self, password):
        self.Password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.Password_hash, password)
    def get_id(self):
        return self.Id
        
@login.user_loader
def load_user(id):
    return Users.query.get(int(id))

class Tracking(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Task = db.Column(db.Integer, db.ForeignKey('tasks.Id'))
    Status = db.Column(db.Integer, db.ForeignKey('statuses.Id'))
    User = db.Column(db.Integer, db.ForeignKey('users.Id'))
    Time = db.Column(db.DateTime())
    StartTime = db.Column(db.DateTime())
    Comment = db.Column(db.String(255))

    
 #   def __repr__(self):
 #       return '<Tracking {}, {}, {}>'.format(self.task, self.comment, self.datetime)

class Tasks(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50))
    Course = db.Column(db.Integer, db.ForeignKey('courses.Id'))
    Type = db.Column(db.Integer, db.ForeignKey('task_types.Id'))
    Developer = db.Column(db.Integer, db.ForeignKey('users.Id'))
    Reviewer = db.Column(db.Integer, db.ForeignKey('users.Id'))
    
    Tracking = relationship('Tracking',backref='task',primaryjoin='Tracking.Task == Tasks.Id')

class Courses(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50))
    FullName = db.Column(db.String(255))
    Department = db.Column(db.Integer, db.ForeignKey('departments.Id'))
    
    CourseBlock = relationship('course_block',backref='course',primaryjoin='course_block.IdCourse == Courses.Id')    
    Task = relationship('Tasks',backref='course',primaryjoin='Tasks.Course == Courses.Id')
    

    #tasks = db.relationship('Tasks', backref='course', lazy='dynamic')

class Blocks(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50))
    
    CourseBlock = relationship('course_block',backref='block',primaryjoin='course_block.IdBlock == Blocks.Id')

class course_block(db.Model):
    Id = db.Column(db.Integer, primary_key=True)    
    IdCourse = db.Column(db.Integer, db.ForeignKey('courses.Id'))
    IdBlock = db.Column(db.Integer, db.ForeignKey('blocks.Id'))

class Statuses(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50))

    Tracking = relationship('Tracking',backref='status',primaryjoin='Tracking.Status == Statuses.Id')

class Departments(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50))
    
    Course = relationship('Courses',backref='department',primaryjoin='Courses.Department == Departments.Id')

class task_types(db.Model):
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(50))

    Task = relationship('Tasks',backref='type',primaryjoin='Tasks.Type == task_types.Id')
    
import os
#basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'a2gf1256756aasd0zzsghkdz089312h12dcxc3vsd98d9a08'  #ключ для защиты от CSRF (ПОМЕНЯТЬ?)
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'mysql://apanovich@fino:ap45rt!@fino.mariadb.database.azure.com:3306/e-courses'
    
    #'mysql://root:123456@127.0.0.1:3306/e-courses'
    #'mysql://apanovich@fino:ap45rt!@fino.mariadb.database.azure.com:3306/e-courses'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_RECYCLE = 15
    TASKS_PER_PAGE = 10
